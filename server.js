const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./models/users.json');
const Game=require('./game');
require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});
  app.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname, 'public/login.html'));
  });
  app.get('/race',passport.authenticate('jwt',{session:false}), function (req, res) {
    res.sendFile(path.join(__dirname, 'models/race.json'));
  });
  app.post('/login', function (req, res) {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.login === userFromReq.login);
    if (userInDB && userInDB.password === userFromReq.password) {
      const token = jwt.sign(userFromReq, 'secret');
      res.status(200).json({ auth: true, token });
    } else {
      res.status(401).json({ auth: false });
    }
  });
  //let players={};
  //let racers={};
  //let text=Math.floor(Math.random() *4);
  const game=new Game();
  game.createRace();
  io.on('connection', socket => {
    socket.on('connection', payload => {//проверка подключения
      const {token} = payload;
      let userLogin = jwt.verify(token,'secret').login;
      if(userLogin){   
        game.setPlayer(socket.id,userLogin);
        socket.broadcast.emit('greet', {player:game.getPlayer(socket.id)});
        socket.emit('greet',{player:game.getPlayer(socket.id),isme:true});
      }
    });
    socket.on('disconnect',()=>{
      socket.broadcast.emit("leaved",{player:game.getPlayers(socket.id)});
      game.deletePlayer(socket.id);
    });
    socket.on("joinrace",payload=>{
      io.of('/').in('race').clients((error, socketIds) => {
        if (error) throw error;
        socketIds.forEach(socketId => io.sockets.sockets[socketId].leave('race'));
      });
      const {token} = payload;
      let userLogin = jwt.verify(token,'secret').login;
      if(userLogin){
          game.getPlayer(socket.id).setTime(payload.time);
          const first=game.getPlayers()[0];
          const now=new Date().getTime()/1000;
          let wait=30;
          const match=()=>{
            if(Object.keys(io.in("race").clients()["sockets"]).length>1){
              if((now-first.time)-(now-payload.time)<=wait) {
                socket.join("race",()=>{
                    player=game.getPlayer(socket.id);
                    game.getLastRace().addPlayer(socket.id,player);
                    io.to("race").emit("you are in",game.bot.greet());
                    io.to("race").emit("your text",game.getLastRace().text);
                    io.emit("your team",game.getLastRace().getPlayers());
                });
              } else {
                socket.emit("unable to join");
              }
            }else{
              wait+=10;
              console.log(wait);
              setTimeout(match,10000);
            }
          } 
          setTimeout(match,1000)
      }
    });
    socket.on("track length",payload=>{
      game.getLastRace().length=payload;
      io.in("race").emit("start",game.bot.countdown());
    });
    socket.on("letter",()=>{
      game.getLastRace().getPlayer(socket.id).incScore();
      io.emit("rating",game.getLastRace().getPlayers());
      if(game.getLastRace().length-game.getLastRace().getPlayer(socket.id).score==30) socket.emit("30tofinish",game.bot.thirtytofinish(game.getLastRace().getPlayer(socket.id).login));
      if(game.getLastRace().getPlayer(socket.id).score==game.getLastRace().length){
        io.emit("win",game.bot.finish(game.getLastRace().getPlayer(socket.id)));
      }
    });
});