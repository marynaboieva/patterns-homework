class Game{
    constructor(){
        this.players={};
        this.races=[];
        this.bot=new Bot();
    }
    setPlayer(socketId,login){
        const player=new Player(login);
        this.players[socketId]=player;
    }
    deletePlayer(socketId){
      delete this.players[socketId];
    }
    getPlayer(socketId){
        return this.players[socketId];
    }
    getPlayers(){
        return Object.values(this.players);
    }
    createRace(){
        let race=new Race();
        this.races.push(race);
    }
    getLastRace(){
        return this.races[this.races.length-1];
    }
}
class Race{
    constructor(){
        this.players={};
        this.text=Math.floor(Math.random() *4);
        this.length=0;
    }
    addPlayer(socketId,player){
      this.players[socketId]=player;
    }
    getPlayer(socketId){return this.players[socketId]}
    getPlayers(){return this.players}
}
class Player{
  constructor(login){
    this.login=login;
    this.score=0;//
    this.time=0;
  }
  setTime(time){this.time=time}
  incScore(){this.score+=1}
}
class Bot{
    constructor(){
        this.text="";
    }
    greet(){
        this.text="Welcome to the new great race!!!My name is Commentator Bot, and I will comment the race events for you";
        return this.text;
    }
    countdown(){
        this.text="3...2...1...GO!!!";
        return this.text;
    }
    thirtytofinish(name){
        this.text=`The race is about to end, and ${name} leads!`;
        return this.text;
    }
    finish(player){
        this.text=`And we have a winner-${player.login} with score ${player.score}!`;
        return this.text;
    }
}
module.exports = Game;