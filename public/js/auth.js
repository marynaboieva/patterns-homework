const jwt = localStorage.getItem('jwt');
    let text="";
    let racers=[];
window.onload = () => {
    if (jwt) {
        const socket = io.connect('http://localhost:3000');
        socket.emit('connection', {token: jwt });
        socket.on('greet', payload => {
            console.log("hi "+payload.player.login);
        });
        socket.on('leaved',payload=>{
            console.log(payload.player.login+"leaved");
        });
        const time=new Date().getTime()/1000;
        socket.emit("joinrace",{token:jwt,time:time});
        socket.on("you are in",payload=>{
            document.getElementById("comment").append(payload);
        });
        socket.on("unable to join",()=>{
            console.log("Please,wait for next race!");
        })
        socket.on("your text",payload=>{
            fetch("/race",{
                method:"GET",
                headers:{'Authorization':'Bearer '+jwt}
            }).then(res => {
                res.json().then(body => {
                    if (body) {
                       text=body[payload].text;
                       socket.emit("track length",text.length); 
                    } else {
                        console.log('auth failed');
                    }
                })
            }).catch(err => {
                console.log('request went wrong');
            });
        });
        socket.on("your team",payload=>{
            racers=[...Object.values(payload)];
            createTeamList(racers);
        });
        socket.on("start",payload=>{
            setTimeout(()=>{
                document.getElementById("comment").innerHTML=payload;
                createTrack(text,socket);
            },4000)
        });
        socket.on("rating",payload=>{
            racers=[...Object.values(payload)];
            createTeamList(racers);
        });
        socket.on("30tofinish",payload=>{
            document.getElementById("comment").innerHTML=payload;
        });
        socket.on("win",payload=>{
            confirm(payload);
            document.getElementById("answer").remove();
        });
    } else {
        location.replace('/login');
    }
}
function createTrack(text,socket){
    const div=document.createElement("div");
    div.id="track";
    for(let i=0;i<text.length;i++){
        const span=document.createElement("span");
        span.innerHTML=text[i];
        span.id=i+"";
        div.appendChild(span);
    }
    const input=document.createElement("textarea");
    input.id="answer";
    document.body.append(div);
    div.append(input);
    input.focus();
    input.style.opacity="0";
    input.addEventListener("keypress",()=>{checkLetter(socket)});
}
function checkLetter(socket){
    const input=document.getElementById("answer");
    let answer=input.value;
    let curr=0;
    if(answer.length>1)curr=answer.length-1;
    else curr=0;
    if (answer[curr]==text[curr]){
        document.getElementById(curr).style.color="green";
        socket.emit("letter");
    } 
    else{
        document.getElementById(curr).style.color="red";
        answer=answer.slice(0,curr);
        input.value=answer;
    } 
}
function createTeamList(team){
    const list=document.getElementById("team");
    list.innerHTML="";
    team.sort((a,b)=>a.score>b.score);
    team.forEach(el=>{
        const li=document.createElement("li");
        li.innerHTML=el.login+":"+el.score;
        document.getElementById("team").appendChild(li);
    });
}